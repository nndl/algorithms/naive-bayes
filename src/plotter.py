"""Common plotting utils"""

# generate scatter plots for the data
from matplotlib import pyplot as plt
import pylab as pl
import numpy as np
def scatter_plot(x, y, xlabel="x", ylabel="y"):
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.xticks(())
    plt.yticks(())
    plt.scatter(x, y, color="g", label="test")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()

def simple_plot(y, ylabel="y"):
    plt.ylim(min(y), max(y))
    plt.xticks(())
    plt.yticks(())
    plt.plot(y)
    plt.ylabel(ylabel)
    plt.show()

def pretty_picture(clf, test_data, filename="ans"):
    x_min = 0.0; x_max = 1.0
    y_min = 0.0; y_max = 1.0
    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    h = 0.01  # step size in the mesh
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())

    plt.pcolormesh(xx, yy, Z, cmap=pl.cm.seismic)

    plt.scatter(test_data["fast"]["grade"], test_data["fast"]["bumpiness"], color = "b", label="fast")
    plt.scatter(test_data["slow"]["grade"], test_data["slow"]["bumpiness"], color = "r", label="slow")
    plt.legend()
    plt.xlabel("bumpiness")
    plt.ylabel("grade")

    plt.savefig("../dist/"+filename+".png")