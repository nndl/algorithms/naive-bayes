"""Generate Terrain Data
This script genearate a random terrain data. 
For reproducability purposes, the seed for the 
pseudo random number generator is passed as a param """

import random, operator

def make_terrain_data(n_points=1000, seed=42):
    random.seed(seed)
    grade = [random.random() for i in range(0,n_points)]
    bumpy = [random.random() for i in range(0,n_points)]
    error = [random.random() for i in range(0,n_points)]
    y = [round(grade[i]*bumpy[i]+0.3+0.1*error[i]) for i in range(0,n_points)]
    for j in range(0, len(y)):
        if grade[j]>0.8 or bumpy[j]>0.8:
            y[j] = 1

    # create a list of tuples from grade & bumpy
    x = [[g, b] for g, b in zip(grade, bumpy)]

    # split the list of tuples 3:1 for training & test
    split = int(0.75*n_points)
    x_train = x[0:split]
    x_test  = x[split:]
    y_train = y[0:split]
    y_test  = y[split:]
    grade_fast = [x_train[i][0] for i in range(0, len(x_train)) if y_train[i]==0]
    bumpy_fast = [x_train[i][1] for i in range(0, len(x_train)) if y_train[i]==0]
    grade_slow = [x_train[i][0] for i in range(0, len(x_train)) if y_train[i]==1]
    bumpy_slow = [x_train[i][1] for i in range(0, len(x_train)) if y_train[i]==1]

    # cleanup and seperation
    training_data = {
        "fast":{
            "grade":grade_fast, 
            "bumpiness":bumpy_fast
        }, 
        "slow":{
            "grade":grade_slow, 
            "bumpiness":bumpy_slow
        }
    }

    grade_fast = [x_test[ii][0] for ii in range(0, len(x_test)) if y_test[ii]==0]
    bumpy_fast = [x_test[ii][1] for ii in range(0, len(x_test)) if y_test[ii]==0]
    grade_slow = [x_test[ii][0] for ii in range(0, len(x_test)) if y_test[ii]==1]
    bumpy_slow = [x_test[ii][1] for ii in range(0, len(x_test)) if y_test[ii]==1]

    test_data = {
        "fast":{
            "grade":grade_fast, 
            "bumpiness":bumpy_fast
        }, 
        "slow":{
            "grade":grade_slow, 
            "bumpiness":bumpy_slow
        }
    }

    return x_train, y_train, x_test, y_test, training_data, test_data
