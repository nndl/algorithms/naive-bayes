""" This is a recreation of the Navie Bayes Algorithm from the 
    Udacity Machine Learning Course, Lesson 2 
    (https://classroom.udacity.com/courses/ud120) 
    implemented on anaconda3-4.3.1 (python3)
    Here, in a simplified version of a decision that a 
    self driving car has to make, we consider 2 variables:
    1. bumpiness: level of unevenness of the terrain
    2. grade: slope of the terrain
    And we have training data that labels the data points 
    with the speed the car is to go at in those condition
    (ie a given set of bumpiness and slope). 
    There's 2 labels: fast & slow"""

from data_generator import make_terrain_data
from classifier import classify
from plotter import pretty_picture

features_train, labels_train, features_test, labels_test, all_training_data, all_test_data = make_terrain_data(1000, 42)
classier, accuracy = classify(features_train, labels_train, features_test, labels_test)
print("accuracy: {:.2f} %".format(100*accuracy))
pretty_picture(classier, all_test_data)