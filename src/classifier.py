"""Classify a given set of tuples and labels (training data)
and measure the accuracy of the classification using test data"""

from sklearn.naive_bayes import GaussianNB

def classify(features_train, labels_train, features_test, labels_test):
    clf = GaussianNB()
    fit = clf.fit(features_train, labels_train)
    acc = clf.score(features_test, labels_test)
    return fit, acc
